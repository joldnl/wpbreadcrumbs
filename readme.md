# Wp Breadcrumbs

A WordPress breadcrumbs class for generatie breadcrumbs in your WordPress project.

This class has been fitted with filters for most of the breadcrumb type items.

## Usage:

Include the class file, and initiate the class with the folowing options:
You can now use this function anywhere in your theme just by calling it: ```<?php echo breadcrumbs(); ?>```.

~~~php
<?php

public function breadcrumbs() {

    $templates = array(
        'before'      => '<nav id="breadcrumb"><ul>',
        'after'       => '</ul></nav>',
        'standard'    => '<li itemscope itemtype="http://schema.org/breadcrumb">%s</li>',
        'current'     => '<li class="current">%s</li>',
        'link'        => '<a href="%s" itemprop="url"><span itemprop="title">%s</span></a>'
    );

    $options = array(
        'show_htfpt' => true,
        'separator'  => '&raquo;',
    );

    $strings = array(
        'home'       => 'Home',
        'paged'      => 'Page %d',
        '404_error'  => 'Page not found',
        'search'     => array(
            'singular'  => 'Search result for <em>"%s"</em>',
            'plural'    => '%s Search results for <em>"%s"</em>'
        )
    );

    return new Breadcrumbs( $templates, $options, $strings );

}
~~~


### Add to TimberSite:

You can also easily add the breadcrumbs to your timber global site context:

~~~php
<?php

public function addToContext($data) {
    $data['breadcrumbs']  = $this->breadcrumbs();
}


public function breadcrumbs() {

    $templates = array(
        'before'      => '<nav id="breadcrumb"><ul>',
        'after'       => '</ul></nav>',
        'standard'    => '<li itemscope itemtype="http://schema.org/breadcrumb">%s</li>',
        'current'     => '<li class="current">%s</li>',
        'link'        => '<a href="%s" itemprop="url"><span itemprop="title">%s</span></a>'
    );

    $options = array(
        'show_htfpt' => true,
        'separator'  => '&raquo;',
    );

    $strings = array(
        'home'       => 'Home',
        'paged'      => 'Page %d',
        '404_error'  => 'Page not found',
        'search'     => array(
            'singular'  => 'Search result for <em>"%s"</em>',
            'plural'    => '%s Search results for <em>"%s"</em>'
        )
    );

    return new Breadcrumbs( $templates, $options, $strings );

}
~~~

This way you can simply get the breadcrumbs anywhere in the site:

~~~twig
{{ breadcrumbs.templates.before }}

{% for item in breadcrumbs.items %}
    {{ item }}
{% endfor %}

{{ breadcrumbs.templates.after }}
~~~


### Function output:

The function returns an object with the folowing structure you van use to iterate trough.

```php
object(Breadcrumbs)
  public 'items' =>
    array
      'home' => '<li><a href="http://www.your-website.com/">Home</a></li>'
      'single_page' => '<li class="current">Page: Contact us</li>'
  public 'templates' =>
    array
      'link' => '<a href="%s">%s</a>'
      'current' => '<li class="current">%s</li>'
      'standard' => '<li>%s</li>'
      'before' => '<nav id="breadcrumb"><ul>'
      'after' => '</ul></nav>'
  public 'strings' =>
    array
      'home' => 'Home'
      'paged' => 'Page %d'
      '404_error' => 'Page not found'
      'search' =>
        array
          'singular' => 'Search result for <em>"%s"</em>'
          'plural' => '%s Search results for <em>"%s"</em>'
  public 'options' =>
    array
      'separator' => '&raquo;'
      'posts_on_front' => false
      'page_for_posts' => '0'
      'show_pagenum' => false
      'show_htfpt' => true
```

### Generate breadcrumbs in templates:

Use the Breadcrumbs->items to loop trough the items.

```php
<?php $breadcrumbs = breadcrumbs(); ?>

<?php echo $breadcrumbs->templates['before']; ?>

<?php foreach( $breadcrumbs->items as $item ):  ?>
	<?php echo $item; ?>
<?php endeach; ?>

<?php echo $breadcrumbs->templates['after']; ?>

```


## Options

The class comes with a few options to manipulate the output of the breadcrumbs:

#### Templates
This array with template options

| Property   | Default value
|------------|------------------------------------
| link       | ```<a href="%s">%s</a>```
| current    | ```<span class="current">%s</span>```
| standard   | ```<span class="standard">%s</span>```
| before     | ```<nav>```
| after      | ```</nav>```


#### Options
Array with breadcrumb options

| Property          | Default value                                                     
|-------------------|-------------------------------------------------------------------
| separator         | ```' › '```                                                       
| posts\_on\_front  | ```'posts' == get_option( 'show_on_front' ) ? true : false```     
| page\_for\_posts  | ```get_option( 'page_for_posts' )```
| show\_pagenum     | ```true``` (support pagination)
| show\_htfpt       | ```false``` (show hierarchical terms for post types)



#### Strings
Array with the default labels for various breadcrumb items

| Property    | Default value                                                     
|-------------|-------------------------------------------------------------------
| home        | ```'Homepage'```                                                       
| paged       | ```'Page %d'```     
| error\_404  | ```'Page not found'```
| search      | ```array(```<br>```'singular' => '%s Search result for <em>%s</em>',```<br>```'plural'   => '%s Search results for <em>%s</em>'```<br>```)```




## Filters

This class has a multitude of filters to help you manipulate the output of the breadcrumb items.

* [firestarter/breadcrumbs/home](https://bitbucket.org/pms72/breadcrumbs#markdown-header-firestarter-breadcrumbs-home)
* [firestarter/breadcrumbs/single](https://bitbucket.org/pms72/breadcrumbs#markdown-header-firestarterbreadcrumbssingle)
* [firestarter/breadcrumbs/search](https://bitbucket.org/pms72/breadcrumbs#markdown-header-firestarter-breadcrumbs-search)
* [firestarter/breadcrumbs/404](https://bitbucket.org/pms72/breadcrumbs#markdown-header-firestarterbreadcrumbs404)
* [firestarter/breadcrumbs/archive/{taxonomy_name}](https://bitbucket.org/pms72/breadcrumbs#markdown-header-firestarterbreadcrumbsarchivetaxonomy_name)
* [firestarter/breadcrumbs/archive/cpt](https://bitbucket.org/pms72/breadcrumbs#markdown-header-firestarterbreadcrumbsarchivecpt)
* [firestarter/breadcrumbs/archive/author](https://bitbucket.org/pms72/breadcrumbs#markdown-header-firestarterbreadcrumbsarchiveauthor)
* [firestarter/breadcrumbs/archive/year](https://bitbucket.org/pms72/breadcrumbs#markdown-header-firestarterbreadcrumbsarchiveyear)
* [firestarter/breadcrumbs/archive/month_year](https://bitbucket.org/pms72/breadcrumbs#markdown-header-firestarterbreadcrumbsarchivemonth_year)
* [firestarter/breadcrumbs/archive/month_month](https://bitbucket.org/pms72/breadcrumbs#markdown-header-firestarterbreadcrumbsarchivemonth_month)
* [firestarter/breadcrumbs/archive/day_year](https://bitbucket.org/pms72/breadcrumbs#markdown-header-firestarterbreadcrumbsarchiveday_year)
* [firestarter/breadcrumbs/archive/day_month](https://bitbucket.org/pms72/breadcrumbs#markdown-header-firestarterbreadcrumbsarchiveday_month)
* [firestarter/breadcrumbs/archive/day_day](https://bitbucket.org/pms72/breadcrumbs#markdown-header-firestarterbreadcrumbsarchiveday_day)



### firestarter/breadcrumbs/home

Filter the label of the homepage item in the breadcrumbs

| Parameters | Type    | Description
|------------|---------|---------------------------------
| $name      | string  | The 'home' string from the settings array

Example usage:

~~~php
<?php

add_filter( 'firestarter/breadcrumbs/home',  'change_home_item' ), 10, 1 );

// Change the home item label into the name of the website
function change_home_item( $name ) {
    return get_bloginfo('name');
}
~~~




### firestarter/breadcrumbs/single

Filter the label of a single post breadcrumb item

| Parameters | Type    | Description
|------------|---------|---------------------------------
| $title     | string  | The title of the single post
| $post      | object  | The WP_Post object of the single post

Example usage:

~~~php
<?php

add_filter( 'firestarter/breadcrumbs/single',  'single_post_title' ), 10, 2 );

// Change the breadcrumb lable for a single post
public function single_post_title( $title, $post ) {

    // Is a WP_Post object?
    if ( get_class($post) == 'WP_Post') {

        $post_type = get_post_type_object( $post->post_type );

        return $post_type->labels->singular_name . ': ' . $title;

    }

    return $title;

}
~~~




### firestarter/breadcrumbs/search

Filter search query title

| Parameters | Type    | Description
|------------|---------|---------------------------------
| $title     | string  | The title of the single post





### firestarter/breadcrumbs/404

Filter the label of a 404 error page

| Parameters   | Type    | Description
|--------------|---------|---------------------------------
| $error_40 4  | string  | The '404_weeoe' string from the settings array

Example usage:

~~~php
<?php

add_filter( 'firestarter/breadcrumbs/404',  'change_404_title' ), 10, 1 );

// Change tot 404 label with an acf options field
public function change_404_title( $error_404 ) {

    $options = [
        'title'     => get_field( 'options_404_title',      'option' ),
        'panorama'  => get_field( 'options_404_panorama',   'option' ),
        'content'   => get_field( 'options_404_content',    'option' ),
    ];

    return $options['title'];

}
~~~





### firestarter/breadcrumbs/archive/{taxonomy_name}

Filter taxonomy name from a taxonomy archive label. Replace _{taxonomy\_name}_ with the name of your targeted taxonomy archive.

| Parameters | Type    | Description
|------------|---------|---------------------------------
| $name      | string  | The default taxonomy name       
| $term_obj  | object  | The WP_Term object of the tax term
| $tax_obj   | object  | The WP_Taxonomy object of the taxonomy

Example usage:

~~~php
<?php

add_filter( 'firestarter/breadcrumbs/archive/{taxonomy_name}',  'change_project_topic' ), 10, 3 );

// Filter the 'project_topic' taxonomy archive label
function change_project_topic( $name, $term_obj, $tax_obj ) {

    // Is a WP_Term object?
    if ( get_class($term_obj) == 'WP_Term' ) {

        $term       = $term_obj->name;

        // Replace 'Project ' from the tax name
        $tax_name   = str_replace( "Project ", "", $tax_obj->labels->singular_name );
        $tax_name   .= ': ' . $term;

        return $tax_name;

    }

    return $name;

}
~~~







### firestarter/breadcrumbs/archive/cpt

Filter post type label from a cutom post type archive label

| Parameters  | Type    | Description
|-------------|---------|---------------------------------
| $label      | string  | The custom post type name
| $object     | object  | The WP_Post_Type object of the custom post type

Example usage:

~~~php
<?php

add_filter( 'firestarter/breadcrumbs/archive/cpt',  'customn_post_type_archive' ), 10, 2 );

// Change the breadcrumb label for custom post type 'project'
public function customn_post_type_archive( $label, $object ) {

    // Is $object a WP_Post_Type object?
    if ( is_object($object) && get_class($object) == 'WP_Post_Type' ) {

        // Check if post type is 'project'
        if ( $object->name == 'project' ) {
            return 'Archive: ' . $object->label . ' (' . wp_count_posts( $object->name )->publish . ')';
        }

    }

    // Return $label by default
    return $label;

}
~~~





### firestarter/breadcrumbs/archive/author

Filter display name of a author archive label

| Parameters      | Type    | Description
|-----------------|---------|---------------------------------
| $display_ name  | string  | The display name of the author
| $user_obj ect   | object  | The WP_User object of the current author archive displayed

Example usage:

~~~php
<?php

add_filter( 'firestarter/breadcrumbs/archive/author',  'author_archive' ), 10, 2 );

// Check for a user role, and display the role in the breadcrumbs
public function author_archive( $display_name, $user_object ) {

    // Is a WP_User object?
    if ( get_class($user_object) == 'WP_User' ) {

        if ( has_user_role('wdcd_maker', $user_object) ) {

            return 'Maker profile: ' . $user_object->first_name . ' ' . $user_object->last_name;

        } elseif ( has_user_role('wdcd_jury', $user_object) ) {

            return 'Jury profile: ' . $user_object->first_name . ' ' . $user_object->last_name;
        }

    }

    return $user_name;

}


// Check if a user has a role from the roles array
public function has_user_role( $role, $user_object ) {

    $roles = $user_object->roles;
    if ( isset($roles) && !empty($roles) && is_array($roles) ) {
        $has_role = array_search($role, $roles);
        if ( $has_role !== false && is_int($has_role) ) {
            return true;
        }
    }

    return false;

}

~~~





### firestarter/breadcrumbs/archive/year

Filter year of a year archive label

| Parameters | Type    | Description
|------------|---------|---------------------------------
| $year      | string  | The year of the archive
| $date      | string  | The wp formatted get\_the\_date()

Example usage:

~~~php
<?php

add_filter( 'firestarter/breadcrumbs/archive/year',  'change_archive_year' ), 10, 2 );

// Add a span with custom text in front of the year label
public function change_archive_year( $year, $date ) {
    return '<span>Year:</span> ' . $year;
}
~~~



### firestarter/breadcrumbs/archive/month_year

Filter year of a month archive label

| Parameters | Type    | Description
|------------|---------|---------------------------------
| $year      | string  | The year of the archive
| $date      | string  | The wp formatted get\_the\_date()

Example usage:

~~~php
<?php

add_filter( 'firestarter/breadcrumbs/archive/month_year',  'change_archive_month_year' ), 10, 2 );

// Add a span with custom text in front of the year label
public function change_archive_month_year( $year, $date ) {
    return '<span>Year:</span> ' . $year;
}
~~~



### firestarter/breadcrumbs/archive/month_month

Filter the month of a month archive label

| Parameters | Type    | Description
|------------|---------|---------------------------------
| $month     | string  | The month of a month archive
| $date      | string  | The wp formatted get\_the\_date()

Example usage:

~~~php
<?php

add_filter( 'firestarter/breadcrumbs/archive/month_month',  'change_archive_month_month' ), 10, 2 );

// Add a span with custom text in front of the month name
public function change_archive_month_month( $month, $date ) {

    $timestamp  = get_the_date( 'U' );      // Unix timestamp of the archive date
    $monthname  = date( 'F', $timestamp );  // Full written month-name
    $monthday   = date( 'j', $timestamp );  // Day of the month

    return '<span>Month:</span> ' . $month;

}
~~~



### firestarter/breadcrumbs/archive/day_year

Filter year of a day archive label

| Parameters | Type    | Description
|------------|---------|---------------------------------
| $year      | string  | The year of a day based archive
| $date      | string  | The wp formatted get\_the\_date()

Example usage:

~~~php
<?php

add_filter( 'firestarter/breadcrumbs/archive/day_year',  'change_archive_day_year' ), 10, 2 );

// Add a span with custom text in front of the year label
public function change_archive_day_year( $year, $date ) {
    return '<span>Year:</span> ' . $year;
}
~~~



### firestarter/breadcrumbs/archive/day_month

Filter year of a year archive label

| Parameters | Type    | Description
|------------|---------|---------------------------------
| $month     | string  | The month of a day based archive
| $date      | string  | The wp formatted get\_the\_date()

Example usage:

~~~php
<?php

add_filter( 'firestarter/breadcrumbs/archive/day_month',  'change_archive_day_month' ), 10, 2 );

// Add a span with custom text in front of the month label
public function change_archive_day_month( $month, $date ) {
    return '<span>Month:</span> ' . $month;
}
~~~





### firestarter/breadcrumbs/archive/day_day

Filter year of a year archive label

| Parameters | Type    | Description
|------------|---------|---------------------------------
| $day       | string  | The day of a day based archive
| $date      | string  | The wp formatted get\_the\_date()

Example usage:

~~~php
<?php

add_filter( 'firestarter/breadcrumbs/archive/day_day',  'change_archive_day_day' ), 10, 2 );

// Rerurn the written weekday with the day of the month with the day of the month
public function change_archive_day_day( $day, $date ) {

    $timestamp  = get_the_date( 'U' );      // Unix timestamp of the archive date
    $weekday    = date( 'l', $timestamp );  // Full written weekday
    $monthday   = date( 'j', $timestamp );  // Day of the month

    if ( $monthday == '1' ) {
        $stth = 'st';
    } if ( $monthday == '2' ) {
        $stth = 'nd';
    } if ( $monthday == '3' ) {
        $stth = 'rd';
    } if ( $monthday > 3 ) {
        $stth = 'th';
    }

    return $weekday . ' ' . $monthday . ' <sup>' . $stth . '</sup>';

}
~~~
