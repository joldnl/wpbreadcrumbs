<?php

/**
 *
 * Breadcrumbs
 *
 * Adds custom breadcrumbs functionality to your site
 *
 * @category     Breadcrumbs
 * @author       Jurgen Oldenburg <info@jold.nl>
 * @version      v1.0.0
 *
 * Filters:
 *
 * firestarter/breadcrumbs/archive/{taxonomy_name}     $name, $term_obj, $tax_obj   Filter taxonomy name from a taxonomy archive label
 * firestarter/breadcrumbs/archive/cpt                 $label, $object              Filter post type label from a cutom post type archive label
 * firestarter/breadcrumbs/archive/author              $names, $object              Filter display name of a author archive label
 * firestarter/breadcrumbs/archive/year                $year                        Filter year of a year archive label
 * firestarter/breadcrumbs/archive/month_year          $year                        Filter year of a month archive label
 * firestarter/breadcrumbs/archive/month_month         $month                       Filter month of a month archive label
 * firestarter/breadcrumbs/archive/day_year            $year                        Filter year of a day archive label
 * firestarter/breadcrumbs/archive/day_month           $month                       Filter month of a day archive label
 * firestarter/breadcrumbs/archive/day_day             $day                         Filter day of a day archive label
 * firestarter/breadcrumbs/search                      $text                        Filter search query title
 * firestarter/breadcrumbs/404                         $404_error                   Filter 404_error string title
 * firestarter/breadcrumbs/single                      $title, $post_obj            Filter a single post
 * firestarter/breadcrumbs/home                        $title                       Filter a single post
 *
 */



/**
 * Main breadcrumbs class
 */
class WpBreadcrumbs {


    /**
     *
     * items
     * The list of breadcrumb items.
     *
     * @var     array
     * @access  public
     * @since   1.0.0
     *
     */
    public $items;



    /**
     *
     * options
     * Templates for link, current/standard state and before/after.
     *
     * @var     array
     * @access  public
     * @since   1.0.0
     *
     */
    public $templates;



    /**
     *
     * options
     * Various strings.
     *
     * @var     array
     * @access  public
     * @since   1.0.0
     *
     */
    public $strings;



    /**
     *
     * options
     * Various options.
     *
     * @var     array
     * @access  public
     * @since   1.0.0
     *
     */
    public $options;



    /**
     *
     * __construct.
     *
     * Main class costructor function
     *
     * @type       function
     * @date       2017/08/07
     * @author     Jurgen Oldenburg
     * @since      1.0.0
     *
     * @param      array    $templates     An array with templates for link, current/standard state and before/after.
     * @param      array    $options       An array with options.
     * @param      array    $strings       An array with strings.
     * @param      bool     $autorun       Autorun or not.
     *
     * @return     string                  The complete html output of the breadcrumbs
     *
     */
    public function __construct( $templates = array(), $options = array(), $strings = array(), $autorun = true ) {

        $this->templates = wp_parse_args(
            $templates,
            array(
                'link'           => '<a href="%s">%s</a>',
                'current'        => '<span class="current">%s</span>',
                'standard'       => '<span class="standard">%s</span>',
                'before'         => '<nav>',
                'after'          => '</nav>'
            )
        );

        $this->options = wp_parse_args(
            $options, array(
                'separator'      => '› ',
                'posts_on_front' => 'posts' == get_option( 'show_on_front' ) ? true : false,
                'page_for_posts' => get_option( 'page_for_posts' ),
                'show_pagenum'   => true, // support pagination
                'show_htfpt'     => false // show hierarchical terms for post types
            )
        );

        $this->strings = wp_parse_args(
            $strings, array(
                'home'           => 'Homepage',
                'paged'          => 'Page %d',
                '404_error'      => 'Page not found',
                'search'         => array(
                    'singular'      => '%s Search result for <em>%s</em>',
                    'plural'        => '%s Search results for <em>%s</em>'
                ),
            )
        );

        // Generate breadcrumb
        if ( $autorun ) {
            // echo $this->output();
            return $this->output();
        }

    }



    /**
     * Return the final breadcrumb.
     *
     * @return string
     */
    public function output() {

        if ( empty( $this->items ) ) {
            $this->generate();
        }

        $items = (string) implode( $this->options['separator'], $this->items );
        return $this->templates['before'] . $items . $this->templates['after'];

    }



    /**
     *
     * template
     *
     * Build the item based on the type.
     *
     * @type	function
     * @date	2017/02/19
     * @since	1.0.0
     * @author  Jurgen Oldenburg
     *
     * @param   string|array    $item
     * @param   string          $type
     *
     * @return  string
     *
     */
    protected function template( $item, $type = 'standard' ) {

        if ( is_array( $item ) ) {
            $type = 'link';
        }

        switch ( $type ) {
            case'link':
                return $this->template(
                    sprintf(
                        $this->templates['link'],
                        esc_url( $item['link'] ),
                        $item['title']
                    )
                );
                break;
            case 'current':
                return sprintf( $this->templates['current'], $item );
                break;
            case 'standard':
                return sprintf( $this->templates['standard'], $item );
                break;
        }
    }




    /**
     *
     * generate_tax_parents
     *
     * Helper to generate taxonomy parents.
     *
     * @type	function
     * @date	2017/02/19
     * @since	1.0.0
     * @author  Jurgen Oldenburg
     *
     * @param   mixed     $term_id
     * @param   mixed     $taxonomy
     *
     * @return  void
     *
     */
    protected function generate_tax_parents( $term_id, $taxonomy ) {

        $parent_ids = array_reverse( get_ancestors( $term_id, $taxonomy ) );

        foreach ( $parent_ids as $parent_id ) {
            $term = get_term( $parent_id, $taxonomy );
            $this->items["archive_{$taxonomy}_{$parent_id}"] = $this->template(
                array(
                    'link' => get_term_link( $term->slug, $taxonomy ),
                    'title' => $term->name
                )
            );
        }

    }



    /**
     *
     * generate
     *
     * Generate the breadcrumbs
     *
     * @type	function
     * @date	2017/02/19
     * @since	1.0.0
     * @author  Jurgen Oldenburg
     *
     * @param   n/a
     *
     * @return  void
     *
     */
    public function generate() {

        $post_type        = get_post_type();
        $queried_object   = get_queried_object();

        $this->options['show_pagenum'] = ( $this->options['show_pagenum'] && is_paged() ) ? true : false;


        // Set homepage defaul (no link)
        $this->items['home'] = $this->template(
            apply_filters( 'firestarter/breadcrumbs/home', $this->strings['home'] ),
            'current'
        );

        // Get homepage url and string
        $home_linked = $this->template(
            array(
                'link'   => home_url( '/' ),
                'title'  => apply_filters( 'firestarter/breadcrumbs/home', $this->strings['home'] ),
            )
        );


        // Set home item with link
        if ( $this->options['posts_on_front'] ) {
            if ( !is_home() || $this->options['show_pagenum'] ) {
                $this->items['home'] = $home_linked;
            }
        }

        else {

            if ( !is_front_page() ) {
                $this->items['home'] = $home_linked;
            }

            if ( is_home() && !$this->options['show_pagenum'] ) {
                $this->items['blog'] = $this->template( get_the_title( $this->options['page_for_posts'] ), 'current' );
            }

            // if ( ( 'post' == $post_type && !is_search() && !is_home() ) || ( 'post' == $post_type && $this->options['show_pagenum'] ) ) {
            //     $this->items['blog'] = $this->template(
            //         array(
            //             'link'   => get_permalink( $this->options['page_for_posts'] ),
            //             'title'  => get_the_title( $this->options['page_for_posts'] )
            //         )
            //     );
            // }

        }

        // Post Type Archive as index
        if ( is_singular() || ( is_archive() && ! is_post_type_archive() ) || is_search() || $this->options['show_pagenum'] ) {
            if ( $post_type_link = get_post_type_archive_link( $post_type ) ) {
                $post_type_label = get_post_type_object( $post_type )->labels->name;
                $this->items["archive_{$post_type}"] = $this->template(
                    array(
                        'link'  => $post_type_link,
                        'title' => $post_type_label
                    )
                );
            }
        }


        // Posts, (sub)pages, attachments and custom post types
        if ( is_singular() && !is_front_page() ) {

            if ( $this->options['show_htfpt'] ) {

                $_id        = $queried_object->ID;
                $_post_type = $post_type;

                if ( is_attachment() ) {
                    // Show terms of the parent page
                    $_id        = $queried_object->post_parent;
                    $_post_type = get_post_type( $_id );
                }

                $taxonomies = get_object_taxonomies( $_post_type, 'objects' );
                $taxonomies = array_values(
                    wp_list_filter(
                        $taxonomies,
                        array( 'hierarchical' => true )
                    )
                );

                if ( !empty($taxonomies) ) {

                    $taxonomy      = $taxonomies[0]->name; // Get the first taxonomy
                    $terms         = get_the_terms( $_id, $taxonomy );

                    if ( !empty($terms) ) {

                        $terms     = array_values( $terms );
                        $term      = $terms[0]; // Get the first term

                        if ( 0 != $term->parent ) {
                            $this->generate_tax_parents( $term->term_id, $taxonomy );
                        }

                        $this->items["archive_{$taxonomy}"] = $this->template(
                            array(
                                'link'  => get_term_link( $term->slug, $taxonomy ),
                                'title' => $term->name
                            )
                        );
                    }
                }
            }


            // Get Parents
            if ( 0 != $queried_object->post_parent ) {

                $parents = array_reverse( get_post_ancestors( $queried_object->ID ) );
                foreach ( $parents as $parent ) {
                    $this->items["archive_{$post_type}_{$parent}"] = $this->template(
                        array(
                            'link'  => get_permalink( $parent ),
                            'title' => get_the_title( $parent )
                        )
                    );
                }

            }

            $this->items["single_{$post_type}"] = $this->template(
                apply_filters( 'firestarter/breadcrumbs/single', get_the_title(), $queried_object ),
                'current'
            );

        }



        // Search
        elseif ( is_search() ) {

            $total = $GLOBALS['wp_query']->found_posts;
            $text  = sprintf(
                _n(
                    $this->strings['search']['singular'],
                    $this->strings['search']['plural'],
                    $total
                ),
                $total,
                get_search_query()
            );

            $this->items['search'] = $this->template(
                apply_filters( 'firestarter/breadcrumbs/search', $text ),
                'current'
            );

            if ( $this->options['show_pagenum'] ) {
                $this->items['search'] = $this->template(
                    array(
                        'link'  => home_url( '?s=' . urlencode( get_search_query( false ) ) ),
                        'title' => apply_filters( 'firestarter/breadcrumbs/search', $text )
                    )
                );
            }

        }



        // All archive pages
        elseif ( is_archive() ) {

            // Categories, Tags and Custom Taxonomies
            if ( is_category() || is_tag() || is_tax() ) {

                $term_obj   = $queried_object;
                $term_name  = $queried_object->taxonomy;
                $tax_obj    = get_taxonomy( $term_name );

                // Get Parents
                if ( 0 != $term_obj->parent && is_taxonomy_hierarchical( $term_obj ) ) {
                    $this->generate_tax_parents( $term_obj->term_id, $term_obj );
                }

                $this->items["archive_{$term_name}"] = $this->template(
                    apply_filters( "firestarter/breadcrumbs/archive/{$term_name}", $term_name, $term_obj, $tax_obj )
                );

                if ( $this->options['show_pagenum'] ) {
                    $this->items["archive_{$term_name}"] = $this->template(
                        array(
                            'link'  => get_term_link( $term_obj->slug, $term_obj ),
                            'title' => apply_filters( "firestarter/breadcrumbs/archive/{$term_obj}", $term_obj->name, $term_obj, $tax_obj )
                        )
                    );
                }

            }


            // Date archive
            elseif ( is_date() ) {

                // Year archive
                if ( is_year() ) {

                    $this->items['archive_year'] = $this->template(
                        apply_filters( 'firestarter/breadcrumbs/archive/year', get_the_date('Y'), get_the_date() ),
                        'current'
                    );

                    if ( $this->options['show_pagenum'] ) {
                        $this->items['archive_year'] = $this->template(
                            array(
                                'link'  => get_year_link( get_query_var( 'year' ) ),
                                'title' => apply_filters( 'firestarter/breadcrumbs/archive/year', get_the_date('Y'), get_the_date() )
                            )
                        );
                    }
                }


                // Month archive
                elseif ( is_month() ) {

                    $this->items['archive_year'] = $this->template(
                        array(
                            'link'  => get_year_link( get_query_var( 'year' ) ),
                            'title' => apply_filters( 'firestarter/breadcrumbs/archive/month_year', get_the_date('Y'), get_the_date() )
                        )
                    );

                    $this->items['archive_month'] = $this->template(
                        apply_filters( 'firestarter/breadcrumbs/archive/month_month', get_the_date('F'), get_the_date() ),
                        'current'
                     );

                    if ( $this->options['show_pagenum'] ) {
                        $this->items['archive_month'] = $this->template(
                            array(
                                'link'  => get_month_link( get_query_var( 'year' ), get_query_var( 'monthnum' ) ),
                                'title' => apply_filters( 'firestarter/breadcrumbs/archive/month_month', get_the_date('F'), get_the_date() )
                            )
                        );
                    }

                }

                // Day archive
                elseif ( is_day() ) {

                    $this->items['archive_year'] = $this->template(
                        array(
                            'link'  => get_year_link( get_query_var( 'year' ) ),
                            'title' => apply_filters( 'firestarter/breadcrumbs/archive/day_year', get_the_date('Y'), get_the_date() )
                        )
                    );

                    $this->items['archive_month'] = $this->template(
                        array(
                            'link'  => get_month_link( get_query_var( 'year' ), get_query_var( 'monthnum' ) ),
                            'title' => apply_filters( 'firestarter/breadcrumbs/archive/day_month', get_the_date('F'), get_the_date() )
                        )
                    );

                    $this->items['archive_day'] = $this->template(
                        apply_filters( 'firestarter/breadcrumbs/archive/day_day', get_the_date('j'), get_the_date() )
                    );

                    if ( $this->options['show_pagenum'] ) {
                        $this->items['archive_day'] = $this->template(
                            array(
                                'link'  => get_month_link(
                                    get_query_var( 'year' ),
                                    get_query_var( 'monthnum' ),
                                    get_query_var( 'day' )
                                ),
                                'title' => apply_filters( 'firestarter/breadcrumbs/archive/day_day', get_the_date('F'), get_the_date() )
                            )
                        );
                    }

                }

            }


            // Custom Post Type Archive
            elseif ( is_post_type_archive() && ! is_paged() ) {
                $post_type_label = get_post_type_object( $post_type )->labels->name;

                $this->items["archive_{$post_type}"] = $this->template(
                    apply_filters(
                        'firestarter/breadcrumbs/archive/cpt',
                        $post_type_label,
                        get_post_type_object( $post_type )
                    ),
                    'current'
                );
            }

            // Author archive
            elseif ( is_author() ) {
                $this->items['archive_author'] = $this->template(
                    apply_filters( 'firestarter/breadcrumbs/archive/author', $queried_object->display_name, $queried_object ),
                    'current'
                );
            }

        }


        // 404 page
        elseif ( is_404() ) {
            $this->items['404'] = $this->template(
                apply_filters( 'firestarter/breadcrumbs/404', $this->strings['404_error'] ),
                'current'
            );
        }

        if ( $this->options['show_pagenum'] ) {
            $this->items['paged'] = $this->template(
                sprintf(
                    $this->strings['paged'],
                    get_query_var( 'paged' )
                ),
                'current'
            );
        }

    }


}
